from flask import Flask, request
from typing import Dict, List, Optional
from datetime import datetime
from pathlib import Path
import os

app = Flask(__name__)

messages: Dict[str, List[str]] = {}


@app.route('/', methods=['POST'])
def input_log():
    key = request.form['key']
    is_title = request.form['is_title'] == 'True'
    level = request.form['level']
    message = request.form['message']

    if key not in messages:
        messages[key] = []
        add_log_message(key, None, f'Remote logging started: {get_current_time_formatted()}', False)

    if is_title:
        messages[key].append("\n")
        add_log_message(key, None, f'==[ {message}] ==', False)
    else:
        add_log_message(key, level, message)

    return 'Stored', 200


@app.route('/flush', methods=['POST'])
def flush():
    key = request.form['key']
    assembly = request.form['assembly']

    if key not in messages:
        return f'No messages for key <{key}> found', 404

    output_path = Path('./output')
    directory_path = Path(output_path / assembly)

    create_dir_if_not_exists(output_path)
    create_dir_if_not_exists(directory_path)

    messages[key].append('\n\n')
    with open(directory_path / f'{key}.log', 'w') as file:
        file.writelines(messages[key])

    print(f'{get_current_time_formatted()} | Logs from {assembly}/{key} flushed')
    messages.pop(key)
    return 'Flushed', 200


def create_dir_if_not_exists(path: Path) -> None:
    if not os.path.exists(path):
        os.makedirs(path)


def add_log_message(key: str, level: Optional[str], message: str, add_arrow: bool = True) -> None:
    line = format_log_line(level, message, add_arrow)
    messages[key].append(line)


def get_current_time_formatted() -> str:
    return datetime.now().strftime('%H:%M:%S.%f')


def format_log_line(level: Optional[str], message: str, add_arrow: bool = True) -> str:
    if add_arrow:
        prefix = '> '
    else:
        prefix = ''

    if level is not None:
        level = f'{level} | '
    else:
        level = ''

    return f'{prefix}{level}{message}\n'


if __name__ == '__main__':
    app.run(port=1337)
