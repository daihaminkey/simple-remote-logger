import glob
import os
import shutil


def clear_dir():
    pattern = './output/*/*.log'
    shutil.rmtree('./output/')


if __name__ == '__main__':
    clear_dir()